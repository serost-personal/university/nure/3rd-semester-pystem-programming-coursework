const std = @import("std");

const cpuinfo = @import("cpuinfo");
const raminfo = @import("raminfo");
const diskinfo = @import("diskinfo");
const gpuinfo = @import("gpuinfo");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    const cpu = try cpuinfo.get(allocator);
    defer cpu.deinit(allocator);

    const ram = try raminfo.get(allocator);
    defer ram.deinit(allocator);

    var disksinfo = try diskinfo.get(allocator);
    defer disksinfo.deinit(allocator);

    var gpusinfo = try gpuinfo.get(allocator);
    defer gpusinfo.deinit(allocator);

    std.debug.print("{}\n", .{cpu});

    std.debug.print(
        "RAM Total: {d} MiB, RAM Avail: {d} MiB\n",
        .{
            ram.total >> 20, // fast bytes to MIB convertion
            ram.available >> 20,
        },
    );

    const maybe_disks = disksinfo.disks;
    if (maybe_disks) |disks| {
        for (disks.items) |disk| {
            std.debug.print("{s}: {}GiB\n", .{
                disk.devname,
                disk.size_bytes >> 30, // fast bytes to GiB convertion
            });

            if (disk.partitions == null)
                continue;

            for (disk.partitions.?.items) |part| {
                std.debug.print(
                    "\t{s}: {}MiB\n",
                    .{
                        part.partname,
                        part.size_bytes >> 20,
                    },
                );
            }
        }
    }

    const maybe_gpus = gpusinfo.gpus;
    if (maybe_gpus) |gpus| {
        const color_depth: u32 = blk: {
            var max_current: u32 = 0;
            for (gpus.items) |*item| {
                if (item.color_depth == null)
                    continue;

                const depth = item.color_depth.?;

                if (depth > max_current) {
                    max_current = depth;
                }
            }
            // pick max
            break :blk max_current;
        };

        const Resolution = gpuinfo.Gpu.Resolution;

        const resolution: Resolution = blk: {
            var max_current = Resolution{ .width = 0, .height = 0 };
            var max_volume: usize = 0;
            // pick max
            for (gpus.items) |*item| {
                if (item.resolution == null)
                    continue;
                const res = item.resolution.?;
                const volume = res.width * res.height;
                if (volume > max_volume) {
                    max_current = res;
                    max_volume = volume;
                }
            }
            break :blk max_current;
        };

        // display things
        for (gpus.items, 0..) |item, i| {
            std.debug.print("GPU[{d}]: {s} {s}\n", .{
                i,
                item.vendor,
                item.product,
            });
        }
        std.debug.print(
            "Color depth: {d}\nResolution: {d}x{d}\n",
            .{
                color_depth,
                resolution.width,
                resolution.height,
            },
        );
    }
}
