const std = @import("std");
const Allocator = std.mem.Allocator;

// for linux
pub fn get(alloc: Allocator) !DisksInfo {
    var sysfs = try std.fs.openIterableDirAbsolute(
        "/sys/block",
        .{}, // use defaults
    );
    defer sysfs.close();

    const disk_prefixes = [_][]const u8{
        "s", // sda, sdb, etc
        "nvme", // nvme0n1, etc
    };

    var walker = try sysfs.walk(alloc);
    defer walker.deinit();

    var disks = DisksInfo.init(alloc);

    // traverse through directory, and find files (dirs) with
    // name that begins with one of within "disk_prefixes"
    while (try walker.next()) |cur| {
        const devname = cur.basename;

        // &disk_prefixes is
        // *const [2][]const u8
        // whereas "*const [2]T" part
        // may be coerced to []const T
        if (!matchesPrefix(devname, &disk_prefixes))
            continue;

        const disk = try Disk.initFromDevname(alloc, devname);
        try disks.disks.?.append(disk);
    }
    return disks;
}

pub const DisksInfo = struct {
    disks: ?std.ArrayList(Disk),

    // can also add init_capacity
    // and use initCapacity instead of init.
    pub fn init(alloc: Allocator) DisksInfo {
        return .{
            .disks = std.ArrayList(Disk).init(alloc),
        };
    }

    pub fn deinit(self: *DisksInfo, alloc: Allocator) void {
        if (self.disks == null) {
            return;
        }
        const disks = self.disks.?;
        // call destructors recursively
        for (disks.items) |*disk| {
            disk.deinit(alloc);
        }
        disks.deinit();
        self.disks = null;
    }
};

pub const Disk = struct {
    size_bytes: usize,
    partitions: ?std.ArrayList(DiskPartition),
    devname: []const u8,

    fn initFromDevname(alloc: Allocator, devname: []const u8) !Disk {
        // now it is owned by Disk
        const dup_devname = try alloc.dupe(u8, devname);
        return Disk{
            .size_bytes = try getSizeBytes(alloc, dup_devname),
            .partitions = try getPartitionsFromDevname(alloc, dup_devname),
            .devname = try alloc.dupe(u8, dup_devname),
        };
    }

    fn getSizeBytes(alloc: Allocator, devname: []const u8) !usize {
        // you can get size in sectors (512 bytes)
        // from /sys/block/*dev-name*/size
        const size_path = try std.fmt.allocPrint(
            alloc,
            "/sys/block/{s}/size",
            .{devname},
        );
        defer alloc.free(size_path);

        const SECTOR_SIZE = 512;
        return SECTOR_SIZE * try readIntFromFile(usize, size_path);
    }

    // seek for partitions in /sys/block/*dev-name*/
    fn getPartitionsFromDevname(
        alloc: Allocator,
        devname: []const u8,
    ) !?std.ArrayList(DiskPartition) {
        const devpath = try std.fmt.allocPrint(
            alloc,
            "/sys/block/{s}",
            .{devname},
        );
        defer alloc.free(devpath);

        var sysfs = try std.fs.openIterableDirAbsolute(
            devpath,
            .{}, // use defaults
        );
        defer sysfs.close();

        var walker = try sysfs.walk(alloc);
        defer walker.deinit();

        var parts = std.ArrayList(DiskPartition).init(alloc);

        while (try walker.next()) |cur| {
            const partname = cur.basename;

            // select only partition directories
            if (!matchesPrefix(
                partname,
                &[1][]const u8{devname},
            ))
                continue;

            const part = try DiskPartition.initFromPartname(
                alloc,
                devname,
                partname,
            );
            try parts.append(part);
        }
        return parts;
    }

    fn deinit(self: *Disk, alloc: Allocator) void {
        if (self.partitions == null) {
            return;
        }
        // call destructor on all partitions
        const parts = self.partitions.?;
        for (parts.items) |*part| {
            part.deinit(alloc);
        }
        parts.deinit();
        self.partitions = null;
        // free devname string
        alloc.free(self.devname);
    }

    const DiskPartition = struct {
        size_bytes: usize,
        devname: []const u8, // has a lifecycle of disk
        partname: []const u8,

        fn deinit(self: *DiskPartition, alloc: Allocator) void {
            alloc.free(self.partname);
        }

        fn initFromPartname(
            alloc: Allocator,
            devname: []const u8,
            partname: []const u8,
        ) !DiskPartition {
            const partname_dup = try alloc.dupe(u8, partname);
            return DiskPartition{
                .size_bytes = try DiskPartition.getSizeBytes(alloc, devname, partname_dup),
                .devname = devname, // has a lifecycle of disk
                .partname = partname_dup,
            };
        }

        fn getSizeBytes(
            alloc: Allocator,
            devname: []const u8,
            partname: []const u8,
        ) !usize {
            // you can get size in sectors (512 bytes)
            // from /sys/block/*dev-name*/*part-name*/size
            const size_path = try std.fmt.allocPrint(
                alloc,
                "/sys/block/{s}/{s}/size",
                .{ devname, partname },
            );
            defer alloc.free(size_path);

            const SECTOR_SIZE = 512;
            return SECTOR_SIZE * try readIntFromFile(usize, size_path);
        }
    };
};

fn matchesPrefix(name: []const u8, prefixes: []const []const u8) bool {
    return for (prefixes) |prefix| {
        if (std.mem.startsWith(u8, name, prefix)) {
            break true;
        }
    } else false;
}

fn readIntFromFile(comptime T: type, abs_filepath: []const u8) !T {
    var file = try std.fs.openFileAbsolute(
        abs_filepath,
        .{},
    );
    defer file.close();

    // you can also do this thing, if size cannot exceed 1024
    // beware of buffer overflow, fool!
    // dont do this in prod, but since it is pet project so whatever
    var _buf = [_]u8{undefined} ** std.mem.page_size;
    var buf: []u8 = &_buf; // *[N]T can be coerced to []T
    buf.len = try file.preadAll(buf, 0);

    if (buf[buf.len - 1] == '\n')
        buf = buf[0 .. buf.len - 1];

    return try std.fmt.parseInt(T, buf, 10);
}
