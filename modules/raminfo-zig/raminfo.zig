const std = @import("std");
const Allocator = std.mem.Allocator;

// for linux
pub fn get(alloc: Allocator) !RamInfo {
    const procfs = try std.fs.openFileAbsolute(
        "/proc/meminfo",
        .{ .mode = std.fs.File.OpenMode.read_only },
    );
    defer procfs.close();

    var reader_buf = std.io.bufferedReader(procfs.reader());
    const reader = reader_buf.reader();

    var line_buf = std.ArrayList(u8).init(alloc);
    defer line_buf.deinit();

    var info: RamInfo = undefined;

    var line_writer = line_buf.writer();
    return while (reader.streamUntilDelimiter(
        line_writer,
        '\n',
        null,
    )) {
        defer line_buf.clearRetainingCapacity();
        const items: []u8 = line_buf.items;
        var iter = std.mem.tokenizeAny(u8, items, ": ");
        const key = iter.next() orelse undefined; // key and value
        const value = iter.next() orelse undefined; // always exist
        if (std.mem.eql(u8, key, "MemTotal")) {
            info.total = try std.fmt.parseInt(@TypeOf(info.total), value, 10);
            info.total *= 1024; // KiB to bytes
        } else if (std.mem.eql(u8, key, "MemAvailable")) {
            info.available = try std.fmt.parseInt(@TypeOf(info.available), value, 10);
            info.available *= 1024; // KiB to bytes
        }
    } else |err| switch (err) {
        error.EndOfStream => return info,
        else => return err,
    };
}

const RamInfo = struct {
    total: usize,
    available: usize,

    const Self = @This();
    pub fn deinit(self: *const Self, alloc: Allocator) void {
        _ = self;
        _ = alloc;
    }
};
