const std = @import("std");

const Allocator = std.mem.Allocator;

pub fn get(alloc: Allocator) !GpusInfo {
    return GpusInfo.initFromLshw(alloc);
}

const GpusInfo = struct {
    raw_json: []const u8,
    gpus: ?std.ArrayList(Gpu),

    fn initFromLshw(alloc: Allocator) !GpusInfo {
        const argv = [_][]const u8{ "lshw", "-C", "Display", "-json" };

        var proc = try std.ChildProcess.exec(.{
            .allocator = alloc,
            .argv = &argv,
        }); // if ok proc.stdout now has json, owned by caller (me!)
        defer alloc.free(proc.stderr);

        return initFromLshwJson(alloc, proc.stdout);
    }

    fn initFromLshwJson(alloc: Allocator, lshw_json: []const u8) !GpusInfo {
        var parser = try std.json.parseFromSlice(
            std.json.Value,
            alloc,
            lshw_json,
            .{},
        );
        defer parser.deinit();

        const ngpus = parser.value.array.items.len;
        if (ngpus == 0) {
            return GpusInfo{
                .raw_json = lshw_json,
                .gpus = null, // no gpus found!
            };
        }

        var gpusinfo = GpusInfo{
            .raw_json = lshw_json,
            .gpus = std.ArrayList(Gpu).init(alloc),
        };

        for (parser.value.array.items) |gpu_json| {
            const vendor = gpu_json.object.get("vendor").?.string;
            const product = gpu_json.object.get("product").?.string;

            const conf = gpu_json.object.get("configuration").?.object;

            var color_depth: ?u32 = null;
            const color_depth_obj = conf.get("depth");
            if (color_depth_obj) |clr| {
                color_depth = try std.fmt.parseInt(u32, clr.string, 10);
            }

            var resolution: ?Gpu.Resolution = null;
            const res_obj = conf.get("resolution");
            if (res_obj) |res| {
                var iter = std.mem.splitAny(u8, res.string, ",");
                resolution = .{
                    .width = try std.fmt.parseInt(u32, iter.next().?, 10),
                    .height = try std.fmt.parseInt(u32, iter.next().?, 10),
                };
            }

            try gpusinfo.gpus.?.append(Gpu{
                .vendor = try alloc.dupe(u8, vendor),
                .product = try alloc.dupe(u8, product),
                .color_depth = color_depth,
                .resolution = resolution,
            });
        }

        return gpusinfo;
    }

    pub fn deinit(self: *GpusInfo, alloc: Allocator) void {
        alloc.free(self.raw_json);
        if (self.gpus == null)
            return;
        for (self.gpus.?.items) |*gpu| {
            gpu.deinit(alloc);
        }
        self.gpus.?.deinit();
    }
};

pub const Gpu = struct {
    vendor: []const u8,
    product: []const u8,

    color_depth: ?u32,
    resolution: ?Resolution,

    pub const Resolution = struct {
        width: u32,
        height: u32,
    };

    fn deinit(self: *Gpu, alloc: Allocator) void {
        alloc.free(self.vendor);
        alloc.free(self.product);
    }
};
