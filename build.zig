const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "sys-prog-coursework",

        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    // N sized array of arrays of 2 byte slices
    const modules = [_][2][]const u8{
        [2][]const u8{ "cpuinfo", "modules/cpuinfo-zig/cpuinfo.zig" },
        [2][]const u8{ "raminfo", "modules/raminfo-zig/raminfo.zig" },
        [2][]const u8{ "diskinfo", "modules/diskinfo-zig/diskinfo.zig" },
        [2][]const u8{ "gpuinfo", "modules/gpuinfo-zig/gpuinfo.zig" },
    };

    for (modules) |module| {
        const cpuinfo_mod = b.addModule(module[0], .{
            .source_file = .{ .path = module[1] },
        });
        exe.addModule(module[0], cpuinfo_mod);
    }

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
